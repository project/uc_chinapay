<?php

/**
 * @file
 * Chinapay RSA key sign and verify.
 */

define ( "DES_KEY", "SCUBEPGW" );
define ( "HASH_PAD", "0001ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff003021300906052b0e03021a05000414" );
$private_key = array ();
function uc_chinapay_hex2bin($hexdata) {
	$bindata = '';
	if (strlen ( $hexdata ) % 2 == 1) {
		$hexdata = '0' . $hexdata;
	}
	for($i = 0; $i < strlen ( $hexdata ); $i += 2) {
		$bindata .= chr ( hexdec ( substr ( $hexdata, $i, 2 ) ) );
	}
	return $bindata;
}
function uc_chinapay_padstr($src, $len = 256, $chr = '0', $d = 'L') {
	$ret = trim ( $src );
	$padlen = $len - strlen ( $ret );
	if ($padlen > 0) {
		$pad = str_repeat ( $chr, $padlen );
		if (strtoupper ( $d ) == 'L') {
			$ret = $pad . $ret;
		} else {
			$ret = $ret . $pad;
		}
	}
	return $ret;
}
function uc_chinapay_bin2int($bindata) {
	$hexdata = bin2hex ( $bindata );
	return uc_chinapay_bchexdec ( $hexdata );
}
function uc_chinapay_bchexdec($hexdata) {
	$ret = '0';
	$len = strlen ( $hexdata );
	for($i = 0; $i < $len; $i ++) {
		$hex = substr ( $hexdata, $i, 1 );
		$dec = hexdec ( $hex );
		$exp = $len - $i - 1;
		$pow = bcpow ( '16', $exp );
		$tmp = bcmul ( $dec, $pow );
		$ret = bcadd ( $ret, $tmp );
	}
	return $ret;
}
function uc_chinapay_bcdechex($decdata) {
	$s = $decdata;
	$ret = '';
	while ( $s != '0' ) {
		$m = bcmod ( $s, '16' );
		$s = bcdiv ( $s, '16' );
		$hex = dechex ( $m );
		$ret = $hex . $ret;
	}
	return $ret;
}
function uc_chinapay_sha1_128($string) {
	$hash = sha1 ( $string );
	$sha_bin = uc_chinapay_hex2bin ( $hash );
	$sha_pad = uc_chinapay_hex2bin ( HASH_PAD );
	return $sha_pad . $sha_bin;
}
function uc_chinapay_rsa_encrypt($private_key, $input) {
	$p = uc_chinapay_bin2int ( $private_key ["prime1"] );
	$q = uc_chinapay_bin2int ( $private_key ["prime2"] );
	$u = uc_chinapay_bin2int ( $private_key ["coefficient"] );
	$dP = uc_chinapay_bin2int ( $private_key ["prime_exponent1"] );
	$dQ = uc_chinapay_bin2int ( $private_key ["prime_exponent2"] );
	$c = uc_chinapay_bin2int ( $input );
	$cp = bcmod ( $c, $p );
	$cq = bcmod ( $c, $q );
	$a = bcpowmod ( $cp, $dP, $p );
	$b = bcpowmod ( $cq, $dQ, $q );
	if (bccomp ( $a, $b ) >= 0) {
		$result = bcsub ( $a, $b );
	} else {
		$result = bcsub ( $b, $a );
		$result = bcsub ( $p, $result );
	}
	$result = bcmod ( $result, $p );
	$result = bcmul ( $result, $u );
	$result = bcmod ( $result, $p );
	$result = bcmul ( $result, $q );
	$result = bcadd ( $result, $b );
	$ret = uc_chinapay_bcdechex ( $result );
	return strtoupper ( uc_chinapay_padstr ( $ret ) );
}
function uc_chinapay_rsa_decrypt($input) {
	global $private_key;
	$check = uc_chinapay_bchexdec ( $input );
	$modulus = uc_chinapay_bin2int ( $private_key ["modulus"] );
	$exponent = uc_chinapay_bchexdec ( "010001" );
	$result = bcpowmod ( $check, $exponent, $modulus );
	$rb = uc_chinapay_bcdechex ( $result );
	return strtoupper ( uc_chinapay_padstr ( $rb ) );
}
function uc_chinapay_buildKey($key) {
	global $private_key;
	if (count ( $private_key ) > 0) {
		foreach ( $private_key as $name => $value ) {
			unset ( $private_key [$name] );
		}
	}
	$ret = false;
	$key_file = parse_ini_file ( $key );
	if (! $key_file) {
		return $ret;
	}
	$hex = "";
	if (array_key_exists ( "MERID", $key_file )) {
		$ret = $key_file ["MERID"];
		$private_key ["MERID"] = $ret;
		$hex = substr ( $key_file ["prikeyS"], 80 );
	} else if (array_key_exists ( "PGID", $key_file )) {
		$ret = $key_file ["PGID"];
		$private_key ["PGID"] = $ret;
		$hex = substr ( $key_file ["pubkeyS"], 48 );
	} else {
		return $ret;
	}
	$bin = uc_chinapay_hex2bin ( $hex );
	$private_key ["modulus"] = substr ( $bin, 0, 128 );
	$cipher = MCRYPT_DES;
	$iv = str_repeat ( "\x00", 8 );
	$prime1 = substr ( $bin, 384, 64 );
	$enc = mcrypt_cbc ( $cipher, DES_KEY, $prime1, MCRYPT_DECRYPT, $iv );
	$private_key ["prime1"] = $enc;
	$prime2 = substr ( $bin, 448, 64 );
	$enc = mcrypt_cbc ( $cipher, DES_KEY, $prime2, MCRYPT_DECRYPT, $iv );
	$private_key ["prime2"] = $enc;
	$prime_exponent1 = substr ( $bin, 512, 64 );
	$enc = mcrypt_cbc ( $cipher, DES_KEY, $prime_exponent1, MCRYPT_DECRYPT, $iv );
	$private_key ["prime_exponent1"] = $enc;
	$prime_exponent2 = substr ( $bin, 576, 64 );
	$enc = mcrypt_cbc ( $cipher, DES_KEY, $prime_exponent2, MCRYPT_DECRYPT, $iv );
	$private_key ["prime_exponent2"] = $enc;
	$coefficient = substr ( $bin, 640, 64 );
	$enc = mcrypt_cbc ( $cipher, DES_KEY, $coefficient, MCRYPT_DECRYPT, $iv );
	$private_key ["coefficient"] = $enc;
	return $ret;
}

function uc_chinapay_sign($msg) {
	global $private_key;
	if (! array_key_exists ( "MERID", $private_key )) {
		return false;
	}
	$hb = uc_chinapay_sha1_128 ( $msg );
	return uc_chinapay_rsa_encrypt ( $private_key, $hb );
}
function uc_chinapay_uc_chinapay_signOrder($merid, $ordno, $amount, $curyid, $transdate, $transtype, $priv1) {
	if (strlen ( $merid ) != 15)
		return false;
	if (strlen ( $ordno ) != 16)
		return false;
	if (strlen ( $amount ) != 12)
		return false;
	if (strlen ( $curyid ) != 3)
		return false;
	if (strlen ( $transdate ) != 8)
		return false;
	if (strlen ( $transtype ) != 4)
		return false;
	$plain = $merid . $ordno . $amount . $curyid . $transdate . $transtype . $priv1;
	return uc_chinapay_sign ( $plain );
}
function uc_chinapay_verify($plain, $check) {
	global $private_key;
	if (! array_key_exists ( "PGID", $private_key )) {
		return false;
	}
	$hb = uc_chinapay_sha1_128 ( $plain );
	$hbhex = strtoupper ( bin2hex ( $hb ) );
	$rbhex = uc_chinapay_rsa_decrypt ( $check );
	return $hbhex == $rbhex ? true : false;
}
function uc_chinapay_uc_chinapay_verifyTransResponse($merid, $ordno, $amount, $curyid, $transdate, $transtype, $ordstatus, $check) {
	if (strlen ( $merid ) != 15)
		return false;
	if (strlen ( $ordno ) != 16)
		return false;
	if (strlen ( $amount ) != 12)
		return false;
	if (strlen ( $curyid ) != 3)
		return false;
	if (strlen ( $transdate ) != 8)
		return false;
	if (strlen ( $transtype ) != 4)
		return false;
	if (strlen ( $ordstatus ) != 4)
		return false;
	if (strlen ( $check ) != 256)
		return false;
	$plain = $merid . $ordno . $amount . $curyid . $transdate . $transtype . $ordstatus;
	return uc_chinapay_verify ( $plain, $check );
}
?>

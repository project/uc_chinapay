<?php

/**
 * @file
 * Chinapay menu items.
 */

/**
 * Ubercart Chinapay complete page.
 */
function uc_chinapay_complete($cart_id = 0) {
  watchdog('chinapay', 'Receiving new order notification for order @order_id.', array('@order_id' => $_POST['Priv1']));

  $order = uc_order_load($_POST['Priv1']);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    return t('An error has occurred during payment. Please contact us to ensure your order has submitted.');
  }

  module_load_include('inc', 'uc_chinapay', 'netpayclient');
  $flag = uc_chinapay_buildKey(variable_get('uc_chinapay_pgpubk', 'PgPubk.key'));
  if (!$flag) {
    echo t("Public key build error");
    exit;
  }
  $flag = uc_chinapay_verifyTransResponse($_POST['merid'], $_POST['orderno'], $_POST['amount'], $_POST['currencycode'], $_POST['transdate'], $_POST['transtype'], $_POST['status'], $_POST['checkvalue']);
  if (!$flag) {
    uc_order_comment_save($order->order_id, 0, t('Attempted unverified chinapay completion for this order.'), 'admin');
    return MENU_ACCESS_DENIED;
  }

  if ($_POST['status'] == '1001' && is_numeric($_POST['amount'])) {
    $comment = t('Paid by chinapay.com order #@order.', array('@order' => $_POST['Priv1']));
    uc_payment_enter($order->order_id, 'chinapay', $_POST['amount'], 0, NULL, $comment);
  }
  else {
    drupal_set_message(t('Your order will be processed as soon as your payment clears at chinapay.com.'));
    uc_order_comment_save($order->order_id, 0, t('Payment is pending approval at chinapay.com.', 'admin'));
  }

  // Empty that cart.
  uc_cart_empty($cart_id);

  // Save changes to order without it's completion.
  uc_order_save($order);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  $page = variable_get('uc_cart_checkout_complete_page', '');

  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}


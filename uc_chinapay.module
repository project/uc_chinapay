<?php

/**
 * @file
 * Integrates chinapay.com's redirected payment service.
 */

/**
 * Implements hook_menu().
 */
function uc_chinapay_menu() {
  $items = array();

  $items['cart/chinapay/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_chinapay_complete',
    'access callback' => 'uc_chinapay_completion_access',
    'type' => MENU_CALLBACK,
    'file' => 'uc_chinapay.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uc_chinapay_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_chinapay_method_title';
  $conf['i18n_variables'][] = 'uc_chinapay_checkout_button';
}

/**
 * Make sure anyone can complete their chinapay orders.
 */
function uc_chinapay_completion_access() {
  return TRUE;
}

/**
 * Implements hook_ucga_display().
 */
function uc_chinapay_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'chinapay' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implements hook_form_alter().
 */
function uc_chinapay_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'chinapay') {
      drupal_add_css(drupal_get_path('module', 'uc_chinapay') . '/uc_chinapay.css');
      unset($form['submit']);
      $form['#suffix'] = drupal_get_form('uc_chinapay_form', $order);
    }
  }
}

/**
 * Implements hook_payment_method().
 *
 * @see uc_payment_method_chinapay()
 */
function uc_chinapay_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_chinapay');

  $methods[] = array(
    'id' => 'chinapay',
    'name' => t('Chinapay'),
    'title' => t('Chinapay:') . '<br /><img src="' . $path . '/chinapay.gif" style="position: relative; left: 2.5em;" />',
    'review' => t('China Pay'),
    'desc' => t('Redirect to chinapay.'),
    'callback' => 'uc_payment_method_chinapay',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Add chinapay settings to the payment method settings form.
 *
 * @see uc_chinapay_payment_method()
 */
function uc_payment_method_chinapay($op, &$arg1) {
  switch ($op) {
    case 'cart-process':
      $_SESSION['pay_method'] = $_POST['pay_method'];
      return;

    case 'settings':
      $form['uc_chinapay_demo'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable demo mode, allowing you to process fake orders for testing purposes using test keys'),
        '#default_value' => variable_get('uc_chinapay_demo', TRUE),
      );
      $form['uc_chinapay_merprk'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant private key'),
        '#description' => t('Your chinapay merchant private key file path. Keep it secret, keep it safe.'),
        '#default_value' => variable_get('uc_chinapay_merprk', ''),
        '#size' => 60,
      );
      $form['uc_chinapay_pgpubk'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant public key'),
        '#description' => t('Your chinapay merchant public key file path.Used to sign cart information. Keep it secret, keep it safe.'),
        '#default_value' => variable_get('uc_chinapay_pgpubk', ''),
        '#size' => 60,
      );

      return $form;
  }
}

/**
 * Form to build the submission to chinapay.com.
 */
function uc_chinapay_form($form_state, $order) {
  module_load_include('inc', 'uc_chinapay', 'netpayclient');
  $merid = uc_chinapay_buildKey(variable_get('uc_chinapay_merprk', 'MerPrK.key'));
  if (!$merid) {
    echo t('Private key build error');
    exit;
  }
  $data = array(
    'MerId' => $merid,
    'OrdId' => sprintf("%'016d", $order->order_id),
    'TransAmt' => sprintf("%'012d", $order->order_total * 100),
    'CuryId' => '156',
    'TransDate' => date('Ymd', time()),
    'TransType' => '0001',
    'Version' => '20070129',
    'BgRetUrl' => url('cart/chinapay/complete/', array('absolute' => TRUE)),
    'PageRetUrl' => url('cart/chinapay/complete/', array('absolute' => TRUE)),
    'Priv1' => $order->order_id,
  );
  $data['ChkValue'] = uc_chinapay_signOrder($data['MerId'], $data['OrdId'], $data['TransAmt'], $data['CuryId'], $data['TransDate'], $data['TransType'], $data['Priv1']);

  $form['#action'] = (variable_get('uc_chinapay_demo', TRUE) ? 'http://payment-test' : 'https://payment') . '.chinapay.com/pay/TransGet';

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Order'),
  );

  return $form;
}


-- Ubercart Chinapay --
This module implements Chinapay payment services for use with Ubercart.

-- Install & Config --
PHP extension mcrypt is dependent, check php info for: mcrypt support => enabled
Enable the module.
Settings: admin/store/settings/payment/edit

-- Collaborate --
If you want to help with this module, you can submit patches, find bugs and
communicate them through the issue queue or even your own Chinapay module 

If you need any special support, 
you can contact with Hibersh (http://hibersh.com)